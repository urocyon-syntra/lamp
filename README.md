# Lamp Stack
LAMP is een acroniem voor een set van gratis vrije softwarepakketten, die vaak samen gebruikt worden om dynamische websites te programmeren:

- Linux, het besturingssysteem;
- Apache HTTP Server, de webserver;
- MariaDB of MySQL, het databasemanagementsysteem (of databaseserver); of in plaats daarvan PostgreSQL (LAPP) of SQLite (LASP)
- P voor PHP, Perl, en/of Python, de programmeertaal


## Linux

Linux is een familie van open-source-, Unix-achtige besturingssystemen gebaseerd op de Linuxkernel. De verschillende Linuxvarianten worden Linuxdistributies genoemd en zijn meestal gratis verkrijgbaar. De Linuxkernel wordt verspreid onder de GNU General Public License (GPL). Omdat Linuxdistributies in de regel gebruikmaken van het GNU-besturingssysteem, de standaard-C-bibliotheek en voldoen aan de POSIX-standaard, wordt het geheel ook wel GNU/Linux genoemd. 

## Apache

Apache HTTP Server is een opensourcewebserver voor Windows, macOS, Linux en andere Unix-achtige besturingssystemen. Apache HTTP Server wordt ook wel Apache of httpd genoemd.

Oorspronkelijk was Apache het enige opensource alternatief voor de Netscape-webserver. Sinds de eerste versie is de populariteit gestaag toegenomen. In 2018 maakt ongeveer 23% van alle websites gebruik van Apache. De door Netcraft uitgevoerde maandelijkse telling van het aantal webservers op het internet wordt door de internetgemeenschap als de meest accurate referentie beschouwd.

Apache wordt gebruikt in combinatie met verschillende databases, template-talen, scripttalen en programmeertalen ten behoeve van webapplicaties. Vooral de modulaire architectuur, die met het uitbrengen van Apache 2.0 sterk verbeterd is, maakt Apache tot een zeer geliefde webserver. De 2.0 release maakt het ook mogelijk om Apache als threaded webserver te gebruiken, wat de prestatie op Windows sterk verbetert. 

## php

PHP (PHP: Hypertext Preprocessor) is een scripttaal, die bedoeld is om op webservers dynamische webpagina's te creëren. PHP is in 1994 ontworpen door Rasmus Lerdorf, een senior softwareontwikkelaar bij IBM. Lerdorf gebruikte Perl als inspiratie.

Aanvankelijk stonden de letters PHP voor Personal Home Page (de volledige naam was Personal Home Page/Forms Interpreter, PHP/FI). Sinds PHP 3.0 is de betekenis een recursief acroniem geworden: PHP: Hypertext Preprocessor. Deze naam geeft aan waar de taal meestal voor gebruikt wordt: informatie verwerken tot hypertext (meestal HyperText Markup Language (HTML) en Extensible HyperText Markup Language (XHTML)). 

```php
<!DOCTYPE html>
<html>
<body>

<?php
$txt1 = "Example php";
$txt2 = "https://www.syntra-ab.be/opleidingen/php-programmeren-basis";
$x = 5;
$y = 4;

echo "<h2>" . $txt1 . "</h2>";
echo "Study PHP at " . $txt2 . "<br>";
echo $x + $y;
?>

</body>
</html>
```

## mysql

MySQL is een propriëtair opensource-managementsysteem voor relationele databases (RDBMS). SQL is de taal die wordt gebruikt om een database van dit systeem op te bouwen, te bevragen en te onderhouden. MySQL werd allereerst vooral gebruikt voor internettoepassingen zoals fora en gastenboeken, meestal in combinatie met PHP. MySQL vormt de basis van vele internettoepassingen en standalone software. 

# installatie

[ Voorbeeld installatie op fedora/centos/Rhel](https://tecadmin.net/install-lamp-apache-mysql-and-php-on-centos-rhel-7/)

# tweak apache

## Prefork MPM
>>>
This Multi-Processing Module (MPM) implements a non-threaded, pre-forking web server. Each server process may answer incoming requests, and a parent process manages the size of the server pool. It is appropriate for sites that need to avoid threading for compatibility with non-thread-safe libraries. It is also the best MPM for isolating each request, so that a problem with a single request will not affect any other.

This MPM is very self-regulating, so it is rarely necessary to adjust its configuration directives.

A single control process is responsible for launching child processes which listen for connections and serve them when they arrive. Apache httpd always tries to maintain several spare or idle server processes, which stand ready to serve incoming requests. In this way, clients do not need to wait for a new child processes to be forked before their requests can be served.
>>>

[Configuratie referentie pagina apache.org Prefrok MPM](https://httpd.apache.org/docs/2.4/mod/prefork.html)

## Worker MPM
>>>
This Multi-Processing Module (MPM) implements a hybrid multi-process multi-threaded server. By using threads to serve requests, it is able to serve a large number of requests with fewer system resources than a process-based server. However, it retains much of the stability of a process-based server by keeping multiple processes available, each with many threads.

A single control process (the parent) is responsible for launching child processes. Each child process creates a fixed number of server threads as specified in the ThreadsPerChild directive, as well as a listener thread which listens for connections and passes them to a server thread for processing when they arrive.
>>>

[Configuratie referentie pagina apache.org Worker MPM](https://httpd.apache.org/docs/2.4/mod/worker.html)

## Event MPM
>>>
The event Multi-Processing Module (MPM) is designed to allow more requests to be served simultaneously by passing off some processing work to the listeners threads, freeing up the worker threads to serve new requests.

event is based on the worker MPM, which implements a hybrid multi-process multi-threaded server. A single control process (the parent) is responsible for launching child processes. Each child process creates a fixed number of server threads as specified in the ThreadsPerChild directive, as well as a listener thread which listens for connections and passes them to a worker thread for processing when they arrive.
>>>

[Configuratie referentie pagina apache.org Event MPM](https://httpd.apache.org/docs/2.4/mod/event.html)

# Caching opties

## Varnish

Enkele voorbeelden van Varnish [^1]

[^1]: https://github.com/nucleus-be/varnish-3.0-configuration-templates/blob/master/production.vcl

# Stack schema
```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "LAMP"
  node1[Varnish] --> node2[apache]
  node2[apache] --> node3[PHP]
  node3[PHP] --> node4[MariaDB]
  node4[MariaDB] --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```
