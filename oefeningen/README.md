# LAMP Stack oefeningen

### Prerequisites

Ter voorbereiding van de les, is het nodig om volgende zaken te voorzien:

1. Installatie van:
	- [Docker toolbox](https://www.docker.com/docker-toolbox)
	- [GIT](http://www.git-scm.com/downloads)
	- Maak je reeds gebruik van Homebrew, dan kan je de gevraagde software makkelijk installeren met:

	```
	brew install git
	brew install caskroom/cask/brew-cask
	brew cask install dockertoolbox
	```

2. Configuratie
	- Start na de installatie de "Docker Quickstart Terminal” op
	- Indien er nog geen default docker-machine bestaat, zal deze applicatie er automatisch één aanmaken.
	- Na de installatie mag je de net aangemaakte docker-machine stoppen met:
		- `docker-machine stop default`
		- De "Docker Quickstart Terminal" mag afgesloten worden met: `exit`.


### Voorbereiding

Wanneer je met Docker in - virtualbox op je systeem werkt heb je vaak last van netwerkproblemen als je van netwerk verandert. De machine even rebooten zou dit moeten verhelpen: `docker-machine restart default`

### Oplossing

De uitgewerkte containers zijn de vinden in deze folder onder de correcte oefening folder

Als je vast zit tijdens een oefening en je wil de volgende vlot kunnen meevolgen kan je de huidige gelijkstellen met de oplossing.
