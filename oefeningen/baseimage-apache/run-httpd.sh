#!/bin/bash 

# Make sure we're not confused by old, incompletely-shutdown httpd
# context after restarting the container.  httpd won't start correctly
# if it thinks it is already running.
rm -rf /run/httpd/* /tmp/httpd*

/usr/local/bin/run 
/usr/local/bin/usage 
#/usr/local/bin/version

exec /usr/sbin/httpd -e debug  -DFOREGROUND -f /etc/httpd/httpd.conf
