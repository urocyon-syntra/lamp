# Oefening 1: Apache
## Omschrijving

In deze oefening gaan we een image bouwen voor een statische web-applicatie.
Hiervoor baseren we ons op de base image OS van vorige week.

### Gegeven bestanden

```

baseimage-apache
├── contrib
│   ├── bin
│   │   ├── run
│   │   ├── usage
│   │   └── version
│   └── etc
│       └── apache
│           ├── conf.d
│           │   ├── default.conf
│           │   ├── info.conf
│           │   ├── languages.conf
│           │   ├── mpm.conf
│           │   ├── proxy.conf
│           │   ├── userdir.conf
│           │   └── vhost.conf
│           ├── httpd.conf
│           └── mime.types
├── Dockerfile
├── README.md
└── run-httpd.sh

5 directories, 15 files

```
##### contrib/
Deze folder bevat alle configuratie-bestanden en scripts voor het bouwen van een apache-image.

###### Dockerfile.oefening
Dit is het bestand dat we gaan opbouwen tijdens deze oefening.

###### Dockerfile.oplossing
Dit bestand bevat een uitgewerkte oplossing van het eind resultaat dat we zouden moeten bekomen na het voltooien van deze oefening.

##### contrib/bin
Deze folder bevat de drie basis scripts die we in elke container plaatsen met basis functies.
- run : de acties die moeten uitgevoerd worden als we de container op starten
- usage : een beschrijving van de functie van de container
- version : informatie over de software versie van de technologie in de container

##### contrib/etc
In deze folder vinden we de basis configuratie van een apache webserver

## Uitwerking Dockerfile
### Stap 1

Open de Dockerfile.oefening met een texteditor naar keuze . Vanaf de volgende stappen gaan we regels toevoegen aan dit bestand.

### Stap 2

```
FROM container-registry.development.syntra.be/baseimage-os:8.0
```

De `FROM`-clause wordt gebruikt voor het definiëren van de image (laag) waarop deze image verder bouwt. Voor deze image gaan we verder bouwen op de baseimage-os image die we vorige week hebben opgebouwd.

```
LABEL maintainer="Niels Vanderbeke"
```
`maintainer=` pas je aan naar je eigen naam.

```
USER root
```

De `USER`-instructie stelt de gebruikersnaam (of UID) en optioneel de gebruikersgroep (of GID) in die moet worden gebruikt bij het uitvoeren van de image en voor alle `RUN`, `CMD` en `ENTRYPOINT` instructies die erop volgen in het Dockerfile.
Vanaf dit punt hebben we root of admin rechten nodig om de OS configuratie aan te passen

```
ENV APACHE_PORT 8080

EXPOSE ${APACHE_PORT}
```

Nieuw in deze Dockerfile is `EXPOSE` waarmee een poort vanuit de container opengezet kan worden.
In dit geval poort 8080 waarop het apache process zal luisteren.

```
RUN microdnf install -y httpd && rm -rf /var/cache/yum && microdnf clean all
```

Zoals we in de vorige les hebben gezien kunnen we met de `RUN`-instructie os commando's uitvoeren.
`microdnf install` laat ons toe software te installeren net zoals `yum install` of `apt-get install`

```
COPY contrib/etc/apache /etc/httpd
```

Via `COPY` kunnen we files in de container kopieren.
Zoals eerder aangegeven vinden we in `contrib/etc/apache` de default apache configuratie.

```
RUN ln -sf /usr/lib64/httpd/modules /var/www/modules && \
    mkdir /var/www/logs && \
    chown 1001:0 -Rf /etc/httpd && \
    chown 1001:0 -Rf /run/httpd && \
    chmod -R ug+rw /etc/httpd && \
    chmod -R ug+rw /run/httpd && \
    find /etc/httpd -type d -exec chmod ug+x {} \;
```

Vermits we de volledige installatie tot op heden met root/admin user hebben uitgevoerd moeten we er voor zorgen dat de runtime-user van de container in staat is de nodige bestanden uit te voeren, lezen en waar nodig kan aanpassen.
Dit beriken we door linux commando's chown en chmod te combineren.
Via de man pagin's van de commando's krijgen jullie een beter inzicht in de functie van boven vermelde comman's en hun parameters.

```
ONBUILD COPY contrib/etc/apache /etc/httpd
ONBUILD COPY contrib/src /app/src
```

De `ONBUILD`-instructie voegt een triggerinstructie toe aan de image  die op een later tijdstip moet worden uitgevoerd, wanneer de image wordt gebruikt als basis voor een andere build.
De trigger wordt uitgevoerd in de context van de downstream-build, alsof deze direct na de `FROM`-instructie in de downstream Dockerfile was ingevoegd.
Dit laat ons toe een baseimage te bouwen die we in volgende projecten kunnen gebruiken om applicatie code te injecteren.

```
ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh
```

De `ADD`-instructie kopieert nieuwe bestanden, mappen of externe bestands-URL's van <src> en voegt ze toe aan het bestandssysteem van de afbeelding op het pad <dest>.
Bovenstaande `ADD` en `RUN` zorgen er voor dat ons runtime script voor apache in de image aanwezig is en dat we dit op het einde van de image via `CMD` kunnen uitvoeren bij het starten van de container.

```
USER 1001
```

Het is een best practice om geen rumtime containers uit te voeren met root/admin rechten in de container.
Daarom veranderen we op het einde van de image altijd terug naar de application of runtime user van de container.

```
CMD ["/run-httpd.sh"]
```

Als laatste statement geven we via `CMD` door wat de container moet doen op het moment dat hij ge\ïnitieerd wordt.


## De images testen

We hebben nu een docker-file. Hiervan gaan we images maken en de werking van de uiteindelijke container verifiëren.

### Stap 1: De images genereren
We gaan eerst onze `apache` image genereren. Hiervoor dien je via de terminal naar de `baseimage-apache`-folder te gaan en voer je het volgende commando uit:

```
docker build --rm -t "baseimage-apache" .
```

Deze instructie zorgt voor heel wat output. Je ziet de verschillende handelingen die docker onderneemt op basis van de instructies in de `Dockerfile`.

Hierboven valt op dat we geen `:<tag>` opgeven. Als er geen tag wordt meegegeven gebruikt Docker standaard `latest`.

### Stap 2: De omgeving opzetten
We hebben nu de images, maar nu dienen we nog onze project-container op te starten, om ons resultaat na te gaan...

```
docker run -d -p 8080:80 --name 'hello_docker' baseimage-apache 
```

### Stap 3: Het resultaat
Wanneer je de *docker-toolbox* gebruikt kan je niet meteen naar localhost:80 surfen om het resultaat te zien.
Je gaat het *IP* moeten achterhalen van je docker-machine.

In de terminal typ je:

```
docker-machine ip default
```

Het resultaat ziet er ongeveer zoals de volgende regel uit:

```
192.168.99.100
```

Wanneer je naar dit ip surft krijg je het test-project te zien.

### Stap 4: Opruimen
Eens je het resultaat hebt kunnen nagaan is het handig om op te ruimen. Zodanig de containers niet in de weg zitten van de volgende oefeningen.

Stop de container:

```
docker stop hello_docker
```

Verwijder de container:

```
docker rm hello_docker
```

